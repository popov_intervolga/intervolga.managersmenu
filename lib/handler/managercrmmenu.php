<?php

namespace Intervolga\ManagersMenu\Handler;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);

class ManagerCrmMenu
{
    public static function makeManagersMenu(&$items)
    {
        global $USER;

        $userGroups = $USER->GetUserGroupArray();
        $managersUserGroupId = Option::get('intervolga.managersmenu', 'MANAGERS_GROUP_ID');
        $isManagerUser =
            !$USER->isAdmin() &&
            in_array($managersUserGroupId, $userGroups);

        if ($isManagerUser) {
            $managersMenuItemsId = array(
                'menu_crm_start' => array('SORT' => 0),
                'menu_crm_lead' => array('SORT' => 1),
                'menu_crm_deal' => array('SORT' => 2),
                'menu_crm_contact' => array('SORT' => 3),
                'menu_crm_company' => array('SORT' => 4),
                'menu_crm_activity' => array('SORT' => 5),
            );

            foreach ($items as $menuItem) {
                $menuItemId = $menuItem['MENU_ID'];
                if (array_key_exists($menuItemId, $managersMenuItemsId)) {
                    $managersMenuItems[$managersMenuItemsId[$menuItemId]['SORT']] = $menuItem;
                }
            }

            ksort($managersMenuItems);
            $items = $managersMenuItems;
        }
    }
}
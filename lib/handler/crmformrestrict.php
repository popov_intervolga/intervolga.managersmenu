<?

namespace Intervolga\ManagersMenu\Handler;

use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class CrmFormRestrict
{
    function doRestriction()
    {
        if (!Loader::includeModule('intervolga.managersmenu')) {
            return;
        }

        if (
            \CSite::InDir('/crm/deal/show/') ||
            \CSite::InDir('/crm/lead/show/') ||
            \CSite::InDir('/crm/contact/show/') ||
            \CSite::InDir('/crm/company/show/')
        ) {
            if (self::isRestricted()) {
                \CJSCore::Init('intervolga_crmshowformrestrict');
            }
        } elseif (
            \CSite::InDir('/crm/deal/edit/') ||
            \CSite::InDir('/crm/lead/edit/') ||
            \CSite::InDir('/crm/contact/edit/') ||
            \CSite::InDir('/crm/company/edit/')
        ) {
            if (self::isRestricted()) {
                \CJSCore::Init('intervolga_crmeditformrestrict');
            }
        }
    }

    protected static function isRestricted()
    {
        global $USER;

        $managersUserGroupId = Option::get('intervolga.managersmenu', 'MANAGERS_GROUP_ID');
        $userGroups = $USER->GetUserGroupArray();

        return !$USER->isAdmin() && in_array($managersUserGroupId, $userGroups);
    }
}
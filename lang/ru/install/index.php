<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['INTERVOLGA_MANAGERSMENU.MODULE_NAME'] = 'Интерволга: Меню для менеджеров';
$MESS['INTERVOLGA_MANAGERSMENU.MODULE_DESC'] = 'Модуль ограничивает различные меню для менеджеров';
$MESS['INTERVOLGA_MANAGERSMENU.PARTNER_NAME'] = 'ИНТЕРВОЛГА';
$MESS['INTERVOLGA_MANAGERSMENU.PARTNER_URI'] = 'http://www.intervolga.ru';
BX.ready(function () {
    var crmOfferInfoRightBtn = document.querySelectorAll('td.crm-offer-info-right-btn');
    crmOfferInfoRightBtn.forEach(function(item, i, arr) {
        item.remove();
    });

    var crmQpvMenuBtn = document.querySelector('.crm-toolbar-btn.crm-title-btn');
    if (!!crmQpvMenuBtn) {
        crmQpvMenuBtn.remove();
    }

    var crmOfferTitleSetWrap = document.querySelectorAll('span.crm-offer-title-set-wrap');
    crmOfferTitleSetWrap.forEach(function(item, i, arr) {
        item.hidden = true;
    });

    var crmOfferEditBtnWrap = document.querySelectorAll('span.crm-offer-edit-btn-wrap');
    crmOfferEditBtnWrap.forEach(function(item, i, arr) {
        item.remove();
    });

    var crmOfferInfoDrgBtn = document.querySelectorAll('td.crm-offer-info-drg-btn');
    crmOfferInfoDrgBtn.forEach(function(item, i, arr) {
        item.hidden = true;
    });

    var sectionButtons = document.querySelectorAll('[id^=section_][id$=_buttons]');
    sectionButtons.forEach(function(item, i, arr) {
        item.hidden = true;
    });

    var crmOfferDrgBtn = document.querySelectorAll('span.crm-offer-drg-btn');
    crmOfferDrgBtn.forEach(function(item, i, arr) {
        item.hidden = true;
    });
});
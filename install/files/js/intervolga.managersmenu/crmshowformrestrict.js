BX.ready(function () {
    var sectionDelete = document.querySelectorAll('span.crm-offer-title-set-wrap');
    sectionDelete.forEach(function(item, i, arr) {
        item.hidden = true;
    });

    var crmOfferDrgBtn = document.querySelectorAll('span.crm-offer-drg-btn');
    crmOfferDrgBtn.forEach(function(item, i, arr) {
        item.hidden = true;
    });

    var crmLeadHeaderInnerMoveBtn = document.querySelectorAll('div.crm-lead-header-inner-move-btn');
    crmLeadHeaderInnerMoveBtn.forEach(function(item, i, arr) {
        item.hidden = true;
    });

    var sectionButtons = document.querySelectorAll('[id^=section_][id$=_buttons]');
    sectionButtons.forEach(function(item, i, arr) {
        item.hidden = true;
    });

    var rightBtns = document.querySelectorAll('.crm-offer-info-right-btn');
    rightBtns.forEach(function(item, i, arr) {
        item.hidden = true;
    });

    var drgBtns = document.querySelectorAll('.crm-offer-info-drg-btn');
    drgBtns.forEach(function(item, i, arr) {
        item.hidden = true;
    });

    var crmQpvMenuBtn = document.querySelector('[id^=crm_][id$=_show_v12_qpv_menu_btn]');
    if (!!crmQpvMenuBtn) {
        crmQpvMenuBtn.remove();
    }

    var bxFormMenu = document.querySelector('.bx-interface-form .bx-crm-interface-form a.bx-context-button.bx-form-menu');
    if (!!bxFormMenu) {
        bxFormMenu.remove();
    }
});
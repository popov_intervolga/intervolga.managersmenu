<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Page\Asset;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages($_SERVER['DOCUMENT_ROOT'] . '/bitrix/templates/bitrix24/components/bitrix/menu/left_vertical/template.php');

$asset = Asset::getInstance();
$asset->addCss('/bitrix/templates/bitrix24/components/bitrix/menu/left_vertical/style.min.css');
$asset->addJs('/bitrix/templates/bitrix24/components/bitrix/menu/left_vertical/script.min.js');

$component->includeComponentTemplate(
    '',
    '/bitrix/templates/bitrix24/components/bitrix/menu/left_vertical'
);
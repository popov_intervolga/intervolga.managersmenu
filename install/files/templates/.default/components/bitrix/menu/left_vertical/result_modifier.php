<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Page\Asset;
use Bitrix\Main\Config\Option;

$asset = Asset::getInstance();

$asset->addCss('/bitrix/templates/bitrix24/components/bitrix/menu/left_vertical/map.min.css');
$asset->addCss('/bitrix/templates/bitrix24/components/bitrix/menu/left_vertical/groups.min.css');
$asset->addJs('/bitrix/templates/bitrix24/components/bitrix/menu/left_vertical/map.min.js');

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/templates/bitrix24/components/bitrix/menu/left_vertical/result_modifier.php');

global $USER;

$userGroups = $USER->GetUserGroupArray();

$managersMenuItemsId = array(
    'menu_crm_favorite' => array('SORT' => 0),
    'menu_live_feed' => array('SORT' => 1),
    'menu_tasks' => array('SORT' => 2),
    'menu_im_messenger' => array('SORT' => 3),
    'menu_calendar' => array('SORT' => 4),
    'menu_files' => array('SORT' => 5),
    'menu_external_mail' => array('SORT' => 6),
    'menu_bizproc_sect' => array('SORT' => 7),
);

$managersUserGroupId = Option::get('intervolga.managersmenu', 'MANAGERS_GROUP_ID');

$isManagerUser =
    !$USER->isAdmin() &&
    in_array($managersUserGroupId, $userGroups);

if ($isManagerUser) {
    $managersMenuItems = array();
    foreach ($arResult['ITEMS']['show'] as $menuItem) {
        $menuItemId = $menuItem['PARAMS']['menu_item_id'];
        if (array_key_exists($menuItemId, $managersMenuItemsId)) {
            $managersMenuItems[$managersMenuItemsId[$menuItemId]['SORT']] = $menuItem;
        }
    }

    foreach ($arResult['ITEMS']['hide'] as $menuItem) {
        $menuItemId = $menuItem['PARAMS']['menu_item_id'];
        if (array_key_exists($menuItemId, $managersMenuItemsId)) {
            $managersMenuItems[$managersMenuItemsId[$menuItemId]['SORT']] = $menuItem;
        }
    }

    unset($arResult['ITEMS']['hide']);
    ksort($managersMenuItems);
    $arResult['ITEMS']['show'] = $managersMenuItems;
}
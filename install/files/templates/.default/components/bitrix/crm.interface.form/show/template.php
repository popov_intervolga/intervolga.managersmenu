<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Page\Asset;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages($_SERVER['DOCUMENT_ROOT'] . '/bitrix/components/bitrix/crm.interface.form/templates/show/template.php');

$component->includeComponentTemplate(
    '',
    '/bitrix/components/bitrix/crm.interface.form/templates/show'
);
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Config\Option;

global $USER;

$userGroups = $USER->GetUserGroupArray();
$managersUserGroupId = Option::get('intervolga.managersmenu', 'MANAGERS_GROUP_ID');
$isManagerUser =
    !$USER->isAdmin() &&
    in_array($managersUserGroupId, $userGroups);

if ($isManagerUser && CSite::InDir('/crm/lead/show/')) {
    $managersTabItems = array(
        'tab_1',
        'tab_live_feed',
        'tab_activity',
        'tab_contact',
        'tab_company',
        'tab_tree',
        'tab_event'
    );

    $managersTabs = array();
    foreach ($arParams['~TABS'] as $tab) {
      if (in_array($tab['id'], $managersTabItems)) {
        $managersTabs[] = $tab;
      }
    }

    $arParams['~TABS'] = $managersTabs;
}
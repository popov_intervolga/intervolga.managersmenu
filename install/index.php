<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Config\Option;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Context;
use Bitrix\Main\EventManager;

class intervolga_managersmenu extends CModule
{
    const MODULE_ID = 'intervolga.managersmenu';
    var $MODULE_ID = self::MODULE_ID;
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    function __construct()
    {
        $arModuleVersion = array();
        include(dirname(__FILE__) . '/version.php');
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];

        $this->MODULE_NAME = Loc::getMessage('INTERVOLGA_MANAGERSMENU.MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('INTERVOLGA_MANAGERSMENU.MODULE_DESC');

        $this->PARTNER_NAME = Loc::getMessage('INTERVOLGA_MANAGERSMENU.PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('INTERVOLGA_MANAGERSMENU.PARTNER_URI');
    }

    function DoInstall()
    {
        ModuleManager::registerModule(self::MODULE_ID);
        Option::set(self::MODULE_ID, 'VERSION_DB', $this->versionToInt());

        $this->InstallEvents();
        $this->InstallFiles();
    }

    function DoUninstall()
    {
        $this->UnInstallEvents();
        $this->UnInstallFiles();

        Option::delete(self::MODULE_ID, 'VERSION_DB');
        ModuleManager::unRegisterModule(self::MODULE_ID);
    }

    function InstallEvents()
    {
        $eventManager = EventManager::getInstance();

        $eventManager->registerEventHandlerCompatible(
            'crm',
            'OnAfterCrmControlPanelBuild',
            self::MODULE_ID,
            '\Intervolga\ManagersMenu\Handler\ManagerCrmMenu',
            'makeManagersMenu'
        );

        $eventManager->registerEventHandlerCompatible(
            'main',
            'OnBeforeProlog',
            self::MODULE_ID,
            '\Intervolga\ManagersMenu\Handler\CrmFormRestrict',
            'doRestriction'
        );
    }

    function UnInstallEvents()
    {
        $eventManager = EventManager::getInstance();

        $eventManager->unRegisterEventHandler(
            'crm',
            'OnAfterCrmControlPanelBuild',
            self::MODULE_ID,
            '\Intervolga\ManagersMenu\Handler\ManagerCrmMenu',
            'makeManagersMenu'
        );

        $eventManager->unRegisterEventHandler(
            'main',
            'OnBeforeProlog',
            self::MODULE_ID,
            '\Intervolga\ManagersMenu\Handler\CrmFormRestrict',
            'doRestriction'
        );
    }

    function InstallFiles()
    {
        $context = Context::getCurrent();
        $server = $context->getServer();
        $documentRoot = $server->getDocumentRoot();

        CopyDirFiles(
            __DIR__ . '/files/templates',
            $documentRoot . '/local/templates',
            true,
            true
        );

        CopyDirFiles(
            __DIR__ . '/files/js',
            $documentRoot . '/local/js',
            true,
            true
        );
    }

    function UnInstallFiles()
    {
        DeleteDirFilesEx('/local/templates/.default/components/bitrix/menu/left_vertical');
		DeleteDirFilesEx('/local/templates/.default/components/bitrix/crm.interface.form/show');
        DeleteDirFilesEx('/local/js/intervolga.managersmenu');
    }

    private function versionToInt()
    {
        return intval(preg_replace('/[^0-9]+/i', '', $this->MODULE_VERSION_DATE));
    }
}
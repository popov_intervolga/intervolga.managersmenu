<?php
defined('B_PROLOG_INCLUDED') || die;

$arModuleVersion = array(
    'VERSION' => '1.0.0',
    'VERSION_DATE' => '2018-02-15 12:00:00'
);
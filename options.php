<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

global $APPLICATION, $USER;

$module_id = 'intervolga.managersmenu';
Loader::includeModule($module_id);

$groupRes = \CGroup::GetListEx(
    array(),
    array(),
    false,
    false,
    array('ID', 'NAME')
);

$userGroups = array();
while($userGroup = $groupRes->fetch()) {
    $userGroups[$userGroup['ID']] = $userGroup['NAME'];
}

$options = array(
    'general' => array(
        Loc::getMessage('INTERVOLGA_MANAGERSMENU_GENERAL'),
        array(
            'MANAGERS_GROUP_ID',
            Loc::getMessage('MANAGERS_GROUP_ID'),
            '',
            array(
                'selectbox',
                $userGroups
            )
        ),
    ),
);

$tabs = array(
    array(
        'DIV' => 'general',
        'TAB' => Loc::getMessage('INTERVOLGA_MANAGERSMENU_TAB_GENERAL'),
        'TITLE' => Loc::getMessage('INTERVOLGA_MANAGERSMENU_TAB_GENERAL')
    ),
);

if ($USER->IsAdmin()) {
    if (check_bitrix_sessid() && strlen($_POST['save']) > 0) {
        foreach ($options as $option) {
            __AdmSettingsSaveOptions($module_id, $option);
        }
        LocalRedirect($APPLICATION->GetCurPageParam());
    }
}

$tabControl = new CAdminTabControl('tabControl', $tabs);
$tabControl->Begin();
?>
<form method="POST"
      action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= htmlspecialcharsbx($mid) ?>&lang=<?= LANGUAGE_ID ?>">
    <? $tabControl->BeginNextTab(); ?>
    <? __AdmSettingsDrawList($module_id, $options["general"]); ?>
    <? $tabControl->Buttons(array("btnApply" => false, "btnCancel" => false, "btnSaveAndAdd" => false)); ?>
    <?= bitrix_sessid_post(); ?>
    <? $tabControl->End(); ?>
</form>
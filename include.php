<?php
defined('B_PROLOG_INCLUDED') || die;

CJSCore::RegisterExt(
    'intervolga_crmshowformrestrict',
    array(
        'js' => '/local/js/intervolga.managersmenu/crmshowformrestrict.js'
    )
);

CJSCore::RegisterExt(
    'intervolga_crmeditformrestrict',
    array(
        'js' => '/local/js/intervolga.managersmenu/crmeditformrestrict.js'
    )
);